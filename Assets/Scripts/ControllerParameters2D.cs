﻿using System;
using UnityEngine;
using System.Collections;

//manipulate in unity inspector window and it can only manipulate fields
[Serializable]
public class ControllerParameters2D
{
	public enum JumpBehaviour
	{
		CanJumpOnGround,
		CanJumpAnywhere,
		CantJump
	};
	
	public Vector2 maxVelocity = new Vector2(float.MaxValue, float.MaxValue);
	
	[Range(0, 90)]
	public float slopeLimit = 30; // climb 30 degrees angle
	
	public float gravity = -25f;
	
	public JumpBehaviour jumpRestrictions;
	
	public float jumpFrequency = 0.25f; //limit how often they can repeatedly jump if they can jump anywhere
	
	public float jumpMagnitude = 12f;
}
