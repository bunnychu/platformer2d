﻿using UnityEngine;
using System.Collections;

public class GiveHealth : MonoBehaviour, IPlayerRespawnListener 
{
	public GameObject effect;
	public int healthPoints;
	
	public void OnTriggerEnter2D (Collider2D col)
	{
		Player player = col.GetComponent<Player>();
		
		if (player == null)
		{
			return;
		}
		
		player.GiveHealth(healthPoints, gameObject);
		Instantiate(effect, transform.position, transform.rotation);
		gameObject.SetActive(false);
	}
	
	public void OnPlayerRespawnAtThisCheckpoint (Checkpoint checkpoint, Player player)
	{
		gameObject.SetActive(true);
	}
}
