﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Path : MonoBehaviour 
{
	public Transform[] points;
	
	public IEnumerator<Transform> GetPathEnumerator()
	{
		if (points == null || points.Length < 1)
		{
			yield break;
		}
		
		int direction = 1;
		int index = 0;
		
		while (true)
		{
			yield return points[index]; //return back the point we're currently looking at 
			
			if (points.Length == 1)
			{
				continue;
			}
				
			if (index <= 0)
			{	
				direction = 1;
			}
			else if (index >= points.Length - 1)
			{
				direction = -1;
			}
			
			index = index + direction;
		}
	}
	
	public void OnDrawGizmos()
	{
		
		if (points == null || points.Length < 2)
		{
			return;
		}
		
		List<Transform> pointsList = points.Where(p => p != null).ToList();
		
		if (pointsList.Count < 2)
		{
			return;
		}
		
		for (int i = 1; i < pointsList.Count; i++)
		{
			Gizmos.DrawLine(pointsList[i - 1].position, pointsList[i].position);
		}
	}
}
