﻿using UnityEngine;
using System.Collections;

public class GiveDamageToPlayer : MonoBehaviour 
{
	public int damageToGive = 10;
	
	private Vector2 _lastPosition;
	private Vector2 _velocity;
	
	public void LateUpdate ()
	{
		_velocity = (_lastPosition - (Vector2) transform.position) / Time.deltaTime;
		_lastPosition = transform.position;
	}
	
	public void OnTriggerEnter2D (Collider2D col)
	{
		Player player = col.GetComponent<Player>();
		
		if(player == null)
		{
			return;
		}
		
		player.TakeDamage(damageToGive, gameObject);
		CharacterController2D controller = player.GetComponent<CharacterController2D>();
		Vector2 totalVelocity = controller.Velocity + _velocity;
		
		controller.SetForce (new Vector2(
			-1 * Mathf.Sign(totalVelocity.x) * Mathf.Clamp(Mathf.Abs(totalVelocity.x) * 6, 10, 40),
			-1 * Mathf.Sign(totalVelocity.y) * Mathf.Clamp(Mathf.Abs(totalVelocity.y) * 2, 5, 30)));
	}
}
