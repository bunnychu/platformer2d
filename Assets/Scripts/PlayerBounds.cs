﻿using UnityEngine;
using System.Collections;

public class PlayerBounds : MonoBehaviour 
{
	public enum BoundsBehaviour
	{
		Nothing,
		Constrain,
		Kill
	};
	
	public BoxCollider2D bounds;
	public BoundsBehaviour above;
	public BoundsBehaviour below;
	public BoundsBehaviour left;
	public BoundsBehaviour right;
	
	private Player _player;
	private BoxCollider2D _boxCollider;
	
	public void Start ()
	{
		_player = GetComponent<Player>();
		_boxCollider = GetComponent<BoxCollider2D>();	
	}
	
	public void Update ()
	{
		if (_player.IsDead)
		{
			return;
		}
		
		Vector2 colliderSize = new Vector2(_boxCollider.size.x * Mathf.Abs (transform.localScale.x), _boxCollider.size.y * Mathf.Abs (transform.localScale.y)) / 2;
		
		if (above != BoundsBehaviour.Nothing && transform.position.y + colliderSize.y > bounds.bounds.max.y)
		{
			ApplyBoundsBehaviour(above, new Vector2(transform.position.x, bounds.bounds.max.y - colliderSize.y));
		}
		
		if (below != BoundsBehaviour.Nothing && transform.position.y - colliderSize.y < bounds.bounds.min.y)
		{
			ApplyBoundsBehaviour(below, new Vector2(transform.position.x, bounds.bounds.min.y + colliderSize.y));
		}
		
		if (right != BoundsBehaviour.Nothing && transform.position.x + colliderSize.x > bounds.bounds.max.x)
		{
			ApplyBoundsBehaviour(right, new Vector2(bounds.bounds.max.x - colliderSize.x, transform.position.y));
		}
		
		if (left != BoundsBehaviour.Nothing && transform.position.x - colliderSize.x < bounds.bounds.min.x)
		{
			ApplyBoundsBehaviour(left, new Vector2(bounds.bounds.min.x + colliderSize.x, transform.position.y));
		}
	}
	
	private void ApplyBoundsBehaviour(BoundsBehaviour behaviour, Vector2 constrainedPosition)
	{
		if (behaviour == BoundsBehaviour.Kill)
		{
			LevelManager.Instance.KillPlayer();
			return;
		}
		
		transform.position = constrainedPosition;
	}
}
