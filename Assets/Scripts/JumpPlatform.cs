﻿using UnityEngine;
using System.Collections;

public class JumpPlatform : MonoBehaviour 
{
	public float jumpMagnitude = 20f;
	public AudioClip hitJumpPlatformSound;
	
	public void ControllerEnter2D(CharacterController2D controller)
	{
		if (hitJumpPlatformSound != null)
		{
			AudioSource.PlayClipAtPoint(hitJumpPlatformSound, transform.position);
		}
		
		controller.SetVerticalForce(jumpMagnitude);
	}
}
