﻿using UnityEngine;
using System.Collections;

public class SimpleEnemyAI : MonoBehaviour, ITakeDamage, IPlayerRespawnListener 
{
	public float speed;
	public float fireRate = 1;
	public int pointsToGivePlayer;
	public Projectile projectile;
	public GameObject destroyedEffect;
	public AudioClip enemyShootSound;
	
	private CharacterController2D _controller;
	private Vector2 _direction;
	private Vector2 _startPosition; //respawned if the player dies
	private float _canFireIn;
	
	public void Start ()
	{
		_controller = GetComponent<CharacterController2D>();
		_direction = new Vector2(-1, 0);
		_startPosition = transform.position;
	}
	
	public void Update ()
	{
		_controller.SetHorizontalForce(_direction.x * speed);
		
		if ((_direction.x < 0 && _controller.State.isCollidingLeft) || (_direction.x > 0 && _controller.State.isCollidingRight))
		{
			_direction = - _direction;
			transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}
		
		if ((_canFireIn -= Time.deltaTime) > 0) //can't fire yet
		{
			return;
		}
		
		RaycastHit2D raycast = Physics2D.Raycast(transform.position, _direction, 10, 1 << LayerMask.NameToLayer("Player"));
		
		if (!raycast)
		{
			return;
		}
		
		Projectile projectileToFire = (Projectile) Instantiate(projectile, transform.position, transform.rotation);
		projectileToFire.Initialize (gameObject, _direction, _controller.Velocity);
		_canFireIn = fireRate;
		
		if (enemyShootSound != null)
		{
			AudioSource.PlayClipAtPoint(enemyShootSound, transform.position);
		}
	}
	
	public void TakeDamage (int damage, GameObject instigator)
	{
		if (pointsToGivePlayer != 0)
		{
			Projectile projectileFired = instigator.GetComponent<Projectile>();
			
			if (projectileFired != null && projectileFired.Owner.GetComponent<Player>() !=null)
			{
				GameManager.Instance.AddPoints(pointsToGivePlayer);
			}
		}
		Instantiate(destroyedEffect, transform.position, transform.rotation);
		gameObject.SetActive(false);
	}
	
	public void OnPlayerRespawnAtThisCheckpoint (Checkpoint checkpoint, Player player)
	{
		_direction = new Vector2(-1, 0);
		transform.localScale = new Vector3(1, 1, 1);
		transform.position = _startPosition;
		gameObject.SetActive(true);
	}
}
