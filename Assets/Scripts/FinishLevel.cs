﻿using UnityEngine;
using System.Collections;

public class FinishLevel : MonoBehaviour 
{
	public string levelName;
	
	public void OnTriggerEnter2D (Collider2D col)
	{
		if (col.GetComponent<Player>() == null)
		{
			return;
		}
		
		LevelManager.Instance.GoToNextLevel(levelName);
	}
}
