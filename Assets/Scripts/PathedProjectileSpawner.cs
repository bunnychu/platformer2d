﻿using UnityEngine;
using System.Collections;

public class PathedProjectileSpawner : MonoBehaviour 
{
	public Transform destination;
	public PathedProjectile projectile;
	public GameObject spawnEffect;
	public AudioClip cannonShootSound;
    public Animator animator;
	
	public float speed;
	public float fireRate;
	
	private float _nextShotInSeconds;
	
	public void Start ()
	{
		_nextShotInSeconds = fireRate;
	}
	
	public void Update ()
	{
		if ((_nextShotInSeconds -= Time.deltaTime) > 0)
		{
			return;
		} 
		
		_nextShotInSeconds = fireRate;
		PathedProjectile pathedProjectile = (PathedProjectile) Instantiate(projectile, transform.position, transform.rotation);
		
		pathedProjectile.Initialize(destination, speed);
		
		if (spawnEffect != null)
		{
			Instantiate(spawnEffect, transform.position, transform.rotation);
		}
		
		if (cannonShootSound != null)
		{
			AudioSource.PlayClipAtPoint(cannonShootSound, transform.position);
		}

	    if (animator != null)
	    {   
            animator.SetTrigger("Fire");
	    }
	}
	
	public void OnDrawGizmos ()
	{
		if (destination == null)
		{
			return;
		}
		
		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, destination.position);
	}
}
