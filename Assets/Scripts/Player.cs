using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour, ITakeDamage 
{
	private bool _isFacingRight;
	private CharacterController2D _controller;
	private float _normalizedHorizontalSpeed; // 1, 0, -1
	private float _canFireIn;
	
	public float maxSpeed = 8f;	
	public float speedAccelerationOnGround = 10f; //how quickly the player goes from moving left to moving right or from not moving to moving
	public float speedAccelerationInAir = 5f;
	public int maxHealth = 100;
	public GameObject damageEffect;
	public Projectile projectile; //reference to the prefab
	public float fireRate;
	public Transform projectileFireLocation;
	public AudioClip playerHitSound;
	public AudioClip playerShootSound;
	public AudioClip playerHealthSound;
    public Animator animator;
	
	public int Health { get; private set; }
	public bool IsDead { get; private set; }
	
	public void Awake ()
	{
		_controller = GetComponent<CharacterController2D>();
		_isFacingRight = transform.localScale.x > 0; //we are facing right if we're not flipped
		Health = maxHealth;
	}
	
	public void Update ()
	{
		_canFireIn -= Time.deltaTime; //everytime we update we set the fire rate smaller
		
		if (!IsDead)
		{
			HandleInput();
		}
		
		float movementFactor = _controller.State.isGrounded ? speedAccelerationOnGround : speedAccelerationInAir;
		
		if (IsDead)
		{
			_controller.SetHorizontalForce(0);
		}
		else
		{
			_controller.SetHorizontalForce(Mathf.Lerp(_controller.Velocity.x, _normalizedHorizontalSpeed * maxSpeed, Time.deltaTime * movementFactor)); //current velocity, maxSpeed determing if it's moving right or moving left,
		}

        animator.SetBool("isGrounded", _controller.State.isGrounded);
        animator.SetFloat("Speed", Mathf.Abs(_controller.Velocity.x) / maxSpeed);
	}
	
	public void FinishLevel ()
	{
		enabled = false;
		_controller.enabled = false;
	}
	public void Kill ()
	{
		_controller.HandleCollisions = false;
		_controller.ResetParameters();
		GetComponent<Collider2D>().enabled = false;
		IsDead = true;
		_controller.SetForce(new Vector2(0, 10));
		Health = 0;
	}
	
	public void RespawnAt (Transform spawnPoint)
	{
		if (!_isFacingRight)
		{
			Flip ();
		}
		
		IsDead = false;
		GetComponent<Collider2D>().enabled = true;
		_controller.HandleCollisions = true;
		transform.position = spawnPoint.position;
		Health = maxHealth;
	}
	
	public void TakeDamage (int damage, GameObject instigator)
	{
		AudioSource.PlayClipAtPoint(playerHitSound, transform.position);
		Instantiate(damageEffect, transform.position, transform.rotation);
		Health -= damage;
		
		if (Health <= 0)
		{
			LevelManager.Instance.KillPlayer();
		}
	}
	
	public void GiveHealth (int healthPoints, GameObject helper)
	{
		AudioSource.PlayClipAtPoint(playerHealthSound, transform.position);
		Health = Mathf.Min (Health + healthPoints, maxHealth);
		
//		if (Health == maxHealth)
//		{
//			return;
//		}
//		
//		if ((Health + healthPoints) >= maxHealth)
//		{
//			Health = maxHealth;
//		}
//		else
//		{
//			Health += healthPoints;
//		}
	}
	
	private void HandleInput ()
	{
		if (Input.GetKey(KeyCode.RightArrow))
		{
			_normalizedHorizontalSpeed = 1; //go to the right
			
			if (!_isFacingRight) //if he's facing left then flip him
			{
				Flip();
			}
		}
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			_normalizedHorizontalSpeed = -1;
			
			if (_isFacingRight)
			{
				Flip();
			}
		}
		else
		{
			_normalizedHorizontalSpeed = 0;
		}
		
		if (_controller.canJump && Input.GetKeyDown(KeyCode.Space))
		{
			_controller.Jump();
		}

        if (Input.GetKeyDown(KeyCode.RightShift))
		{
			FireProjectile();
		}
	}
	
	private void FireProjectile ()
	{
		if (_canFireIn > 0) //we can't fire anything at this point
		{
			return;
		}	
		
		Vector2 direction = _isFacingRight ? Vector2.right : -Vector2.right;
		Projectile projectileToFire = (Projectile) Instantiate (projectile, projectileFireLocation.position, projectileFireLocation.rotation);
		projectileToFire.Initialize(gameObject, direction, _controller.Velocity);
		//projectileToFire.transform.localScale = new Vector3(_isFacingRight ? 1 : -1, 1, 1);
		
		_canFireIn = fireRate;
        AudioSource.PlayClipAtPoint(playerShootSound, transform.position);
        
        animator.SetTrigger("Fire");
	}
	
	private void Flip ()
	{
		transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		_isFacingRight = transform.localScale.x > 0;
	}
}
