﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	public Transform player;
	
	public Vector2 margin;
	public Vector2 smoothing;
	
	public BoxCollider2D boundsCam;
	
	private Vector3 _min;
	private Vector3 _max;
	
	public bool isFollowing { get; set;}
	
	public void Start ()
	{
		_min = boundsCam.bounds.min;
		_max = boundsCam.bounds.max;
		isFollowing = true;
	}
	
	public void Update ()
	{
		float x = transform.position.x;
		float y = transform.position.y;
		
		if (isFollowing)
		{
			if(Mathf.Abs(x - player.position.x) > margin.x)
			{
				x = Mathf.Lerp(x, player.position.x, smoothing.x * Time.deltaTime);
			}
			
			if(Mathf.Abs(y - player.position.y) > margin.y)
			{
				y = Mathf.Lerp(y, player.position.y, smoothing.y * Time.deltaTime);
			}
		}
		
		float cameraHalfWidth = GetComponent<Camera>().orthographicSize * ((float) Screen.width / Screen.height);
		
		x = Mathf.Clamp(x, _min.x + cameraHalfWidth, _max.x - cameraHalfWidth);
		y = Mathf.Clamp(y, _min.y + GetComponent<Camera>().orthographicSize, _max.y - GetComponent<Camera>().orthographicSize); //ortsize half the height of the camera in units
		
		transform.position = new Vector3(x, y, transform.position.z);
	}
}
