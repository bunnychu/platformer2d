﻿using System;
using UnityEngine;
using System.Collections;

//keep track of the state of the controller and also they are proprieties because we do not intend to manipulate them in the unity inspector window
public class ControllerState2D 
{
	public bool isCollidingRight { get; set; }
	public bool isCollidingLeft { get; set; }
	public bool isCollidingAbove { get; set; }
	public bool isCollidingBelow { get; set; }
	public bool isMovingDownSlope { get; set; }
	public bool isMovingUpSlope { get; set; }
	public bool isGrounded { get { return isCollidingBelow; } }  //a better terminology for that particular propriety
	public bool isDead { get; set; }
	public float slopeAngle { get; set; }
	
	public bool hasCollisions { get {return isCollidingRight || isCollidingLeft || isCollidingAbove || isCollidingBelow; } }
	
	public void Reset ()
	{
		isMovingUpSlope =
			isMovingDownSlope =
			isCollidingLeft =
			isCollidingRight =
			isCollidingAbove =
			isCollidingBelow = false;
			
		slopeAngle = 0;
	}
	
	public override string ToString ()
	{
		return string.Format (
			"[ControllerState2D: isCollidingRight={0}, isCollidingLeft={1}, isCollidingAbove={2}, isCollidingBelow={3}, isMovingDownSlope={4}, isMovingUpSlope={5}, isGrounded={6}, slopeAngle={7}, hasCollisions={8}]", 
			isCollidingRight, 
			isCollidingLeft, 
			isCollidingAbove, 
			isCollidingBelow, 
			isMovingDownSlope, 
			isMovingUpSlope, 
			isGrounded, 
			slopeAngle, 
			hasCollisions);
	}
}
