﻿
public interface IPlayerRespawnListener  
{
	void OnPlayerRespawnAtThisCheckpoint (Checkpoint checkpoint, Player player);
}
