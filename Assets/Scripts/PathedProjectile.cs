﻿using UnityEngine;
using System.Collections;

public class PathedProjectile : MonoBehaviour, ITakeDamage 
{
	private Transform _destination;
	private float _speed;
	
	public GameObject destroyEffect;
	public int pointsToGivePlayer;
	public AudioClip destroySound;
	
	public void Initialize (Transform destination, float speed)
	{
		_destination = destination;
		_speed = speed;
	}
	
	public void Update ()
	{
		transform.position = Vector3.MoveTowards(transform.position, _destination.position, _speed * Time.deltaTime);
		
		float distanceSquared = (_destination.transform.position - transform.position).sqrMagnitude;
		
		if (distanceSquared > 0.01f * 0.01f)
		{
			return;
		}
		
		if (destroyEffect != null)
		{
			Instantiate (destroyEffect, transform.position, transform.rotation);
		}
		
		if (destroySound != null)
		{
			AudioSource.PlayClipAtPoint(destroySound, transform.position);
		}
		
		Destroy(gameObject);
	}
	
	public void TakeDamage (int damage, GameObject instigator)
	{
		if (destroyEffect != null)
		{
			Instantiate(destroyEffect, transform.position, transform.rotation); 
		}
		
		Destroy(gameObject);
		
		Projectile projectile = instigator.GetComponent<Projectile>();
		
		if (projectile != null && projectile.Owner.GetComponent<Player>() != null && pointsToGivePlayer != 0)
		{
			GameManager.Instance.AddPoints(pointsToGivePlayer);
		}
	}
}
