﻿using UnityEngine;
using System.Collections;

public class PointStar : MonoBehaviour, IPlayerRespawnListener 
{
	public GameObject effect;
	public int points = 10;
	public AudioClip collectStarSound;
    public Animator animator;
    public SpriteRenderer renderer;

    private bool _isCollected;
	
	public void OnTriggerEnter2D (Collider2D col)
	{
	    if (_isCollected)
	    {
	        return;
	    }

	    if (col.GetComponent<Player>() == null)
		{
			return;
		}
		
		if (collectStarSound != null)
		{
			AudioSource.PlayClipAtPoint(collectStarSound, transform.position);
		}
		
		GameManager.Instance.AddPoints(points);
		Instantiate(effect, transform.position, transform.rotation);

	    _isCollected = true;
        animator.SetTrigger("Collect");
		//gameObject.SetActive(false);
	}

    public void FinishAnimationEvent()
    {
        //gameObject.SetActive(false);
        renderer.enabled = false;
        animator.SetTrigger("Reset");
    }
	#region IPlayerRespawnListener implementation

	public void OnPlayerRespawnAtThisCheckpoint (Checkpoint checkpoint, Player player)
	{
	    _isCollected = false;
	    renderer.enabled = true;
	    //gameObject.SetActive(true);
	}

	#endregion
}
