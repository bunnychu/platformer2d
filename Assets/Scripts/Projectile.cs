﻿using UnityEngine;
using System.Collections;

public abstract class Projectile : MonoBehaviour 
{
	public float speed;
	public LayerMask collisionMask;
	
	public GameObject Owner { get; set; }
	public Vector2 Direction { get; set; }
	public Vector2 InitialVelocity { get; private set; }
	
	public void Initialize (GameObject owner, Vector2 direction, Vector2 initialVelocity)
	{
		transform.right = direction;
		Owner = owner;
		Direction = direction;
		InitialVelocity = initialVelocity;
		OnInitialized();
	}
	
	protected virtual void OnInitialized () //overridable by the children
	{
	
	}
	
	public virtual void OnTriggerEnter2D (Collider2D col)
	{
		// LayerNr = Binary    = Decimal
		// Layer 0 = 0000 0001 = 1
		// Layer 1 = 0000 0010 = 2
		// Layer 2 = 0000 0100 = 4
		// Layer 3 = 0000 1000 = 8
		// Layer 4 = 0001 0000 = 16
		// Layer 5 = 0010 0000 = 32
		// Layer 6 = 0100 0000 = 64
		// Layer 7 = 1000 0000 = 128
		// col.gameObject.layer - the layer number itself (0 to 31)
		// layerMask.value the binary representation of the combination of some layers (ex layer 6,5,2,1 = 0110 0110)
		// we want to compare to a layer, for example, is layer 5 in the mask?
		
		
		// to find out we need to turn the concept of layer 5 in binary (1 << 5) 
		// 0000 0001 << 5 = 0010 0000
		// 0110 0110 & 0010 0000 = 0010 0000
		
		if ((collisionMask.value & (1 << col.gameObject.layer)) == 0) //if the object we collided with matches our collision 
		{
			OnNotCollideWith(col); //if it doesn;t then we do this
			return;
		}
		
		bool isOwner = col.gameObject == Owner;
		
		if (isOwner)
		{
			OnCollideOwner();
			return;
		}
		//we have to use the non generic form and not GetComponent<ITakeDamage>
		ITakeDamage takeDamage = (ITakeDamage) col.GetComponent(typeof (ITakeDamage)); //extract the interface (ITakeDamage the object is capable of taking damage)
		
		if (takeDamage != null) //if successful
		{
			OnCollideTakeDamage(col, takeDamage);
			return;
		}
		
		OnCollideOther(col); //if none of the above things happened
	}
	
	protected virtual void OnNotCollideWith (Collider2D col) //an optional thing for children of this class to implement if they choose to do something when the collision mask doesn;t match
	{
	
	}
	
	protected virtual void OnCollideOwner ()
	{
	
	}
	
	protected virtual void OnCollideTakeDamage (Collider2D col, ITakeDamage takeDamage)
	{
	
	}
	
	protected virtual void OnCollideOther (Collider2D col)
	{
		
	}
}
