﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class LevelManager : MonoBehaviour 
{
	public static LevelManager Instance { get; private set; }
	
	public Player Player { get; private set; }
	public CameraController CameraController { get; private set; }
	public TimeSpan RunningTime { get { return DateTime.UtcNow - _started; } } // now - when we started the last checkpoint
	
	public int CurrentTimeBonus
	{
		get 
		{
			int secondsDifference  = (int)(bonusCutoffSeconds - RunningTime.TotalSeconds); //
			return Mathf.Max(0, secondsDifference) * bonusSecondsMultiplier;
		}
	}
	private List<Checkpoint> _checkpoints;
	private int _currentCheckpointIndex;
	private DateTime _started;
	private int _savedPoints;
	
	public Checkpoint debugSpawn;
	public int bonusCutoffSeconds; //max amount of time while still receiving one bonus time (any time after that and you won't receive a bonus)
	public int bonusSecondsMultiplier; //indicates how many seconds left over * how many points
	
	public void Awake ()
	{
		_savedPoints = GameManager.Instance.Points;
		Instance = this; 
	}
	
	public void Start ()
	{
		_checkpoints = FindObjectsOfType<Checkpoint>().OrderBy(t => t.transform.position.x).ToList();
		_currentCheckpointIndex = _checkpoints.Count > 0 ? 0 : -1; //-1 special flag to determine there are no checkpoints
		
		Player = FindObjectOfType<Player>();
		CameraController = FindObjectOfType<CameraController>();
		
		_started = DateTime.UtcNow;
		
		IEnumerable<IPlayerRespawnListener> listeners = FindObjectsOfType<MonoBehaviour>().OfType<IPlayerRespawnListener>();
		foreach (var listener in listeners)
		{
			for (int i = _checkpoints.Count - 1; i >= 0; i--) //looping backwards
			{
				float distance = ((MonoBehaviour)listener).transform.position.x - _checkpoints[i].transform.position.x;
				
				if (distance < 0)
				{
					continue;
				}
				
				_checkpoints[i].AssignObjectToCheckPoint(listener);
				break;
			}
		}
		
#if UNITY_EDITOR
		if (debugSpawn != null) //if we've set a debug spawn then tell the debug spawn to spawn the player
		{
			debugSpawn.SpawnPlayer(Player);
		} 
		else if (_currentCheckpointIndex != -1) //otherwise set the current checkpoint to spawn the player
		{
			_checkpoints[_currentCheckpointIndex].SpawnPlayer(Player);
		}
#else
		if (_currentCheckpointIndex != -1)
		{
			_checkpoints[_currentCheckpointIndex].SpawnPlayer(Player);
		}
#endif
	}
	
	public void Update ()
	{
		bool isAtLastCheckpoint = _currentCheckpointIndex + 1 >= _checkpoints.Count;
		
		if (isAtLastCheckpoint)
		{
			return;
		}
		
		float distanceToNextCheckPoint = _checkpoints[_currentCheckpointIndex + 1].transform.position.x - Player.transform.position.x;
		
		if (distanceToNextCheckPoint >= 0)
		{
			return;
		}
		
		_checkpoints[_currentCheckpointIndex].PlayerLeftCheckPoint();
		_currentCheckpointIndex++;
		_checkpoints[_currentCheckpointIndex].PlayerHitCheckPoint();
		
		GameManager.Instance.AddPoints(CurrentTimeBonus);
		_savedPoints = GameManager.Instance.Points;
		_started = DateTime.UtcNow;
	}
	
	public void GoToNextLevel (string levelName)
	{
		StartCoroutine(GoToNextLevelCo(levelName));
	}
	
	private IEnumerator GoToNextLevelCo (string levelName)
	{
		Player.FinishLevel();
		GameManager.Instance.AddPoints(CurrentTimeBonus);
		yield return new WaitForSeconds(2f);
		
		if (string.IsNullOrEmpty(levelName))
		{
			Application.LoadLevel("Start");
		}
		else
			Application.LoadLevel(levelName);
	}
	
	public void KillPlayer ()
	{
		StartCoroutine(KillPlayerCoroutine()); //allow a method execution to span multiple frames
	}
	
	private IEnumerator KillPlayerCoroutine ()
	{
		Player.Kill();
		CameraController.isFollowing = false;
		yield return new WaitForSeconds(2f);
		
		CameraController.isFollowing = true;
		
		if (_currentCheckpointIndex != -1)
		{
			_checkpoints[_currentCheckpointIndex].SpawnPlayer(Player);
		}
		
		_started = DateTime.UtcNow;
		GameManager.Instance.ResetPoints(_savedPoints);
	}
}
