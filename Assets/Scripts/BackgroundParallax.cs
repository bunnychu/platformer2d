﻿using UnityEngine;
using System.Collections;

public class BackgroundParallax : MonoBehaviour //based on the position of itself (camera)
{
	public Transform[] backgrounds;
	public float parallaxScale;
	public float parallaxReductionFactor;
	public float smoothing;
	
	private Vector3 _lastPosition; //how much we need to scroll the bg
	
	public void Start ()
	{
		_lastPosition = transform.position;
	}
	
	public void Update ()
	{
		float parallax = (_lastPosition.x - transform.position.x) * parallaxScale;
		
		for (int i = 0; i < backgrounds.Length; i++)
		{
			float backgroundTargetPosition = backgrounds[i].position.x + parallax * (i * parallaxReductionFactor + 1); //1 in the case i = 0;
			backgrounds[i].position = Vector3.Lerp(
				backgrounds[i].position, 
				new Vector3(backgroundTargetPosition, backgrounds[i].position.y, backgrounds[i].position.z), 
				smoothing * Time.deltaTime);
			
			_lastPosition = transform.position;
		}
	}
}
