﻿using System;
using UnityEngine;
using System.Collections;

public class CharacterController2D : MonoBehaviour 
{
	private const float skinWidth = 0.02f;
	private const int totalHorizontalRays = 8;
	private const int totalVerticalRays = 4;
	
	private static readonly float slopeLimitTangent = Mathf.Tan(75f * Mathf.Deg2Rad);
	
	public LayerMask platformMask; // layermask that's going to be used for the collision detection (the only things that we collide are things that match the platform mask). That means we won't collide with a star or health pack for example
	public ControllerParameters2D defaultParameters;
	
	public ControllerState2D State { get; private set; }
	public Vector2 Velocity { get {return _velocity;} }
	public bool HandleCollisions { get; set; }
	public ControllerParameters2D Parameters { get {return _overrideParameteres ?? defaultParameters;} } //It returns the left-hand operand if the operand is not null; otherwise it returns the right hand operand.
	
	public GameObject StandingOn { get; private set; }
	public Vector3 PlatformVelocity { get; private set; }
	
	public bool canJump 
	{ 
		get 
		{
			if (Parameters.jumpRestrictions == ControllerParameters2D.JumpBehaviour.CanJumpAnywhere) 
			{
				return _jumpIn <= 0;
			}
			
			if (Parameters.jumpRestrictions == ControllerParameters2D.JumpBehaviour.CanJumpOnGround)
			{
				return State.isGrounded;
			}
			
			return false;
		} 
	}
	
	private Vector2 _velocity;
	private Transform _transform;
	private Vector3 _localScale;
	private BoxCollider2D _boxCollider;
	private ControllerParameters2D _overrideParameteres;
	private float _jumpIn;
	private GameObject _lastStandingOn; 
	
	private Vector3 _activeGlobalPlatformPoint;
	private Vector3 _activeLocalPlatformPoint;
	private Vector3 _raycastTopLeft;
	private Vector3 _raycastBottomLeft;
	private Vector3 _raycastBottomRight;
	
	private float _verticalDistanceBetweenRays; //
	private float _horizontalDistanceBetweenRays; 
	
	public void Awake ()
	{
		HandleCollisions = true;
		State = new ControllerState2D();
		_transform = transform;
		_localScale = transform.localScale;
		_boxCollider = GetComponent<BoxCollider2D>();
		
		float colliderWidth = _boxCollider.size.x * Mathf.Abs(_localScale.x) - (2 * skinWidth); // if our player is flipped we need to multiply it with the mathf.abs of the localScale 
		_horizontalDistanceBetweenRays = colliderWidth / (totalVerticalRays - 1);
		
		float colliderHeight = _boxCollider.size.y * Mathf.Abs(_localScale.y) - (2 * skinWidth);
		_verticalDistanceBetweenRays = colliderHeight / (totalHorizontalRays - 1);
	}
	
	public void AddForce (Vector2 force)
	{
		_velocity += force;
	}
	
	public void SetForce (Vector2 force)
	{
		_velocity = force;
	}
	
	public void SetHorizontalForce (float x)
	{
		_velocity.x = x; //references the field and we can manipulate components of
	}
	
	public void SetVerticalForce (float y)
	{
		_velocity.y = y;
	}
	
	public void Jump ()
	{
		//TODO: Moving platform support
		AddForce(new Vector2(0, Parameters.jumpMagnitude));
		_jumpIn = Parameters.jumpFrequency;
	}
	
	public void LateUpdate ()  //most of the logic here so we can be sure all of the updates of the other objects have been invoked
	{
		_jumpIn -= Time.deltaTime;
		_velocity.y += Parameters.gravity * Time.deltaTime;
		Move (Velocity * Time.deltaTime); // move the character per his velocity scaled by time
	}
	
	private void Move (Vector2 deltaMovement)
	{
		bool wasGrounded = State.isCollidingBelow;
		State.Reset();
		
		if (HandleCollisions)
		{
			HandlePlatforms();
			CalculateRayOrigins(); //where our rays is going to originate from in an absolute sense (that changes on every frame because it takes into acc player position)
			
			if (deltaMovement.y < 0 && wasGrounded) //if it's moving down, if it's affected by gravity
			{
				//we need to handle vertical slope
				HandleVerticalSlope(ref deltaMovement); //may modify the delta movement
			}
			
			if (Mathf.Abs(deltaMovement.x) > 0.001f) // if they're moving to the left or to the right, then move horizontally
			{
				MoveHorizontally(ref deltaMovement);
			}
			
			MoveVertically(ref deltaMovement);
			
			CorrectHorizontalPlacement(ref deltaMovement, true);
			CorrectHorizontalPlacement(ref deltaMovement, false);
		}	
		
		_transform.Translate(deltaMovement, Space.World);
		
		// TODO: Additional moving platform code
		
		if (Time.deltaTime > 0)
		{
			_velocity = deltaMovement / Time.deltaTime;
		}
		
		_velocity.x = Mathf.Min (_velocity.x, Parameters.maxVelocity.x);
		_velocity.y = Mathf.Min (_velocity.y, Parameters.maxVelocity.y); //clamping
		
		if (State.isMovingUpSlope)
		{
			_velocity.y = 0;
		}
		
		if (StandingOn != null)
		{
			_activeGlobalPlatformPoint = transform.position; 
			_activeLocalPlatformPoint = StandingOn.transform.InverseTransformPoint(transform.position); //calculate the velocity of any obj that we're standing on , we're storing were the platform was
			
			Debug.DrawLine(transform.position, _activeGlobalPlatformPoint, Color.blue);
			Debug.DrawLine(transform.position, _activeLocalPlatformPoint, Color.white);
			
			if (_lastStandingOn != StandingOn)
			{
				if (_lastStandingOn != null)
				{
					_lastStandingOn.SendMessage("ControllerExit2D", this, SendMessageOptions.DontRequireReceiver);
				}
				StandingOn.SendMessage("ControllerEnter2D", this, SendMessageOptions.DontRequireReceiver);
				_lastStandingOn = StandingOn;
			}
			else if (StandingOn != null)
			{
				StandingOn.SendMessage("ControllerStay2D", this, SendMessageOptions.DontRequireReceiver);
			}
		}
		else if (_lastStandingOn != null)
		{
			_lastStandingOn.SendMessage("ControllerExit2D", this, SendMessageOptions.DontRequireReceiver);
			_lastStandingOn = null;
		}
	}
	
	private void HandlePlatforms ()
	{
		if (StandingOn != null) // if we were standing on something in the last frame
		{
			Vector3 newGlobalPlatformPoint = StandingOn.transform.TransformPoint(_activeLocalPlatformPoint); //calculate a new global platform point by transform
			Vector3 moveDistance = newGlobalPlatformPoint - _activeGlobalPlatformPoint;
			
			if (moveDistance != Vector3.zero)
			{
				transform.Translate(moveDistance, Space.World);  //put the player and stick him to the platform
			}
			
			PlatformVelocity = (newGlobalPlatformPoint - _activeGlobalPlatformPoint) / Time.deltaTime; // platform velocity ..new point substracting the old point dividing it by time
		}
		else
		{
			PlatformVelocity = Vector3.zero;			
		}
		
		StandingOn = null; //reset our standing on back to null
	}
	
	private void CorrectHorizontalPlacement (ref Vector2 deltaMovement, bool isRight)
	{
		float halfWidth = (_boxCollider.size.x * _localScale.x) / 2f;
		Vector2 rayOrigin = isRight ? _raycastBottomRight : _raycastBottomLeft;
		
		if (isRight)
		{
			rayOrigin.x -= (halfWidth - skinWidth);
		}
		else
		{
			rayOrigin.x += (halfWidth - skinWidth);
		}
		
		Vector2 rayDirection = isRight ? Vector2.right : -Vector2.right;
		float offset = 0f;
		
		for (int i = 0; i < totalHorizontalRays - 1; i++ ) //we're going from the second ray
		{
			Vector2 rayVector = new Vector2(deltaMovement.x + rayOrigin.x, deltaMovement.y + rayOrigin.y + (i * _verticalDistanceBetweenRays)); //how much change is gonna happen in the y before doing the calculation so it's not off when going frame by frame
			Debug.DrawRay(rayVector, rayDirection * halfWidth, isRight ? Color.green : Color.magenta);
			
			RaycastHit2D raycastHit = Physics2D.Raycast(rayVector, rayDirection, halfWidth, platformMask);
			
			if (!raycastHit)
			{
				continue;
			}
			
			offset = isRight ? ((raycastHit.point.x - _transform.position.x) - halfWidth) : (halfWidth - (_transform.position.x - raycastHit.point.x));
		}
		
		deltaMovement.x += offset;
		
	}
	private void CalculateRayOrigins () //precomputing where the ray are going to be shot out from (raycast 
	{
		Vector2 size = new Vector2 (_boxCollider.size.x * Mathf.Abs (_localScale.x), _boxCollider.size.y * Mathf.Abs (_localScale.y)) / 2;
		Vector2 center = new Vector2 (_boxCollider.offset.x * _localScale.x, _boxCollider.offset.y * _localScale.y);
		
		_raycastTopLeft = _transform.position + new Vector3 (center.x - size.x + skinWidth, center.y + size.y - skinWidth, 0f);
		//_raycastTopRight = _transform.position + new Vector2 (center.x + size.x - skinWidth, center.y + size.y - skinWidth);
		_raycastBottomLeft = _transform.position + new Vector3 (center.x - size.x + skinWidth, center.y - size.y + skinWidth, 0f);
		_raycastBottomRight = _transform.position + new Vector3 (center.x + size.x - skinWidth, center.y - size.y + skinWidth, 0f);
		
	}
	
	private void MoveHorizontally (ref Vector2 deltaMovement) //references deltaMovement because it needs to manipulate it based o the existence of a platform to the right or left of the player
	{
		bool isGoingRight = deltaMovement.x > 0; //are we going right
		float rayDistance = Mathf.Abs(deltaMovement.x) + skinWidth; 
		Vector2 rayDirection = isGoingRight ? Vector2.right : -Vector2.right; 
		Vector2 rayOrigin = isGoingRight ? _raycastBottomRight : _raycastBottomLeft;
		
		for (int i = 0; i < totalHorizontalRays; i++)
		{
			Vector2 rayVector = new Vector2(rayOrigin.x, rayOrigin.y + (i * _verticalDistanceBetweenRays));
			Debug.DrawRay(rayVector, rayDirection * rayDistance, Color.red);
			
			RaycastHit2D raycastHit = Physics2D.Raycast(rayVector, rayDirection, rayDistance, platformMask);
			
			if (!raycastHit)
			{
				continue;
			}
			
			if (i == 0 && HandleHorizontalSLope(ref deltaMovement, Vector2.Angle(raycastHit.normal, Vector2.up), isGoingRight ))
			{
				break;
			}
			
			deltaMovement.x  = raycastHit.point.x - rayVector.x; //if we hit something we can only move that far up foprward (take the point and subtract it by ray vector)
			rayDistance = Mathf.Abs(deltaMovement.x);
			
			if (isGoingRight)
			{
				deltaMovement.x -= skinWidth;
				State.isCollidingRight = true;
			}
			else
			{
				deltaMovement.x += skinWidth;
				State.isCollidingLeft = true;
			}
			
			if (rayDistance < skinWidth + 0.001f)
			{
				break;
			}
		}
	}
	
	private void MoveVertically ( ref Vector2 deltaMovement) //our object will always try to move vertically every frame because of gravity (can't go through the roof, or can't fall through the ground)
	{
		bool isGoingUp = deltaMovement.y > 0;
		float rayDistance = Mathf.Abs(deltaMovement.y) + skinWidth;
		Vector2 rayDirection = isGoingUp ? Vector2.up : -Vector2.up;
		Vector2 rayOrigin = isGoingUp ? _raycastTopLeft : _raycastBottomLeft;
		
		rayOrigin.x += deltaMovement.x; 
		
		float standingOnDistance = float.MaxValue;
		
		for (int i = 0; i < totalVerticalRays; i++)
		{
			Vector2 rayVector = new Vector2(rayOrigin.x + ( i *  _horizontalDistanceBetweenRays), rayOrigin.y);
			Debug.DrawRay(rayVector, rayDirection * rayDistance, Color.red);
			
			RaycastHit2D raycastHit = Physics2D.Raycast(rayVector, rayDirection, rayDistance, platformMask);
			
			if (!raycastHit) // if there wasn't a hit
			{
				continue;
			}
			
			if (!isGoingUp) //we keep track of what platform we're standing on
			{
				float verticalDistanceToHit = _transform.position.y - raycastHit.point.y;
				
				if (verticalDistanceToHit < standingOnDistance)
				{
					standingOnDistance = verticalDistanceToHit;
					StandingOn = raycastHit.collider.gameObject;
				}
			}
			
			deltaMovement.y = raycastHit.point.y - rayVector.y;
			rayDistance = Mathf.Abs (deltaMovement.y);
			
			if (isGoingUp) 
			{
				deltaMovement.y -= skinWidth;
				State.isCollidingAbove = true;
			}
			else
			{
				deltaMovement.y += skinWidth;
				State.isCollidingBelow = true;
			}
			
			if (!isGoingUp && deltaMovement.y > 0) //handling slopes
			{
				State.isMovingUpSlope = true;
			}
		
			if (rayDistance < skinWidth + 0.001f)
			{
				break;
			}
		}
		
	}
	
	private void HandleVerticalSlope (ref Vector2 deltaMovement) //handling the verSlope will allow us to move up slopes, vertical movement on the slope
	{
		float center = (_raycastBottomLeft.x + _raycastBottomRight.x) / 2; // center where we're casting our vertical rays
		Vector2 direction = -Vector2.up;
		
		float slopeDistance = slopeLimitTangent * (_raycastBottomRight.x - center);
		Vector2 slopeRayVector = new Vector2(center, _raycastBottomLeft.y);
		
		Debug.DrawRay(slopeRayVector, direction * slopeDistance, Color.yellow);
		RaycastHit2D raycastHit = Physics2D.Raycast(slopeRayVector, direction, slopeDistance, platformMask);
		
		if (!raycastHit)
		{
			return;
		}
		
		bool isMovingDownSlope = Mathf.Sign(raycastHit.normal.x) == Mathf.Sign (deltaMovement.x); // 1 nr pozitiv, -1 negativ, 0
		
		if (!isMovingDownSlope)
		{
			return;
		}
		
		float angle = Vector2.Angle(raycastHit.normal, Vector2.up);
		
		if (Math.Abs (angle) < 0.001f) // we're not actually to a slope, we'on on something that's perpendicular to us
		{
			return;
		}
		
		State.isMovingDownSlope = true;
		State.slopeAngle = angle;
		deltaMovement.y = raycastHit.point.y - slopeRayVector.y;
	}
	
	private bool HandleHorizontalSLope (ref Vector2 deltaMovement, float angle, bool isGoingRight) 
	{
		if (Mathf.RoundToInt(angle) == 90) // we don;t want to move up an angle that's 90
		{
			return false;
		}
		
		if (angle > Parameters.slopeLimit) // too steep for us, we just early exit out;
		{
			deltaMovement.x = 0;
			return true;
		}
		
		if (deltaMovement.y > 0.07f) //we're moving up
		{
			return true;
		}
		
		deltaMovement.x += isGoingRight ? -skinWidth : skinWidth;
		deltaMovement.y = Mathf.Abs(Mathf.Tan(angle * Mathf.Deg2Rad) * deltaMovement.x); // we modify it based on the angle of the slope we're moving up
		State.isMovingUpSlope = true;
		State.isCollidingBelow = true;
		
		return true;
	}
	
	public void OnTriggerEnter2D (Collider2D other)
	{
		ControllerPhysicsVolume2D parametersVolume = other.gameObject.GetComponent<ControllerPhysicsVolume2D>();
		
		if (parametersVolume == null)
		{
			return;
		}
		
		_overrideParameteres = parametersVolume.parameters;
		
	}
	
	public void OnTriggerExit2D (Collider2D other)
	{
		ControllerPhysicsVolume2D parametersVolume = other.gameObject.GetComponent<ControllerPhysicsVolume2D>();
		
		if (parametersVolume == null)
		{
			return;
		}
		
		_overrideParameteres = null;
	}
	
	public void ResetParameters ()
	{
		_overrideParameteres = null;
	}
}
